package krosmocraft.sorts;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import cc.moecraft.cooldownapi.Cooldown;
import krosmocraft.interactions.main;
import krosmocraft.krosmobjects.Personnage;
import krosmocraft.krosmobjects.Players;

public abstract class Sort {
	
	protected ItemStack s = new ItemStack(Material.MAGMA_CREAM, 1);//this is the spell's icon
	protected ItemStack s2 = new ItemStack(Material.MAGMA_CREAM, 1);//this is the spell's icon when it's in cooldown
	protected String name;
	protected ArrayList<String> lore = new ArrayList<String>();//this is the spell's description
	protected int id;
	protected int cooldown; 
	protected int slot;
	protected int portee;
	protected main Main;

	protected abstract boolean cast(Player p);
	
	public Sort(main Main) {
		this.Main = Main;
	}
	
	public String getName() {
		return s.getItemMeta().getDisplayName();
	}
	
	public ItemStack getIcon(Player p) {
		if (Cooldown.isInCooldown(p.getUniqueId(), "" + id)) {
			ItemStack i = s2.clone();
			i.setAmount(Cooldown.getTimeLeft(p.getUniqueId(), "" + id));
			return i;
		}
		return s.clone();
	}
	
	public ItemStack getIcon() {
		return s.clone();
	}
	
	public ItemStack getIconCd() {
		return s2.clone();
	}
	
	public int getId() {
		return id;
	}
	
	public int getSlot() {
		return slot;
	}
	
	//this is called when a spell instance is created
	public void create() {
		ItemMeta m = s.getItemMeta();
		m.setDisplayName(name); 
		m.setLore(lore);
		m.setCustomModelData(id);
	    m.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
	    s.setItemMeta(m);
		m = s2.getItemMeta();
		m.setDisplayName(name); 
		m.setLore(lore);
		m.setCustomModelData(id + 1000);
	    m.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
	    s2.setItemMeta(m);
	    Main.Sorts.put(id, this);
	}
	
	//this registers the events inside of every spell that implements event listeners
	public void registerEvents() {
	    if (this instanceof Listener) {
			Main.getServer().getPluginManager().registerEvents((Listener) this, Main);
	    }
	}
	
	//this is the method called when the player presses the corresponding spell hotkey
	public boolean use(Player p) { 
		if (cooldown == -1) {
    		p.sendMessage(ChatColor.DARK_GREEN + "Le passif " + name + " est actif");
		}
		else if (Cooldown.isInCooldown(p.getUniqueId(), "" + id)) {
			if (!checkInvocation(p)) {
				p.sendMessage(ChatColor.DARK_RED + "Vous devez attendre encore " + Cooldown.getTimeLeft(p.getUniqueId(), "" + id) + " secondes avant de pouvoir lancer ce sort.");
			}
		}
		else if (!Main.stunned.contains(p.getUniqueId())){
			if (Main.silenced.contains(p.getUniqueId())) {
				p.sendMessage(ChatColor.DARK_RED + "Impossible de lancer ce sort, vous êtes sous silence.");
			}
			else if (cast(p)){
				cooldown(p);
				return true;
			}
		}
		return false;
	}
	
	protected void cooldown(Player p) {
		if (this.cooldown == 0) {
			return;
		}
		Cooldown cd;
		if(!Main.Dofus.ocre.contains(p.getUniqueId())) {
			cd = new Cooldown(p.getUniqueId(), "" + id, cooldown);
		}
		else {
			if (id != 32 && id != 44 && id != 104) {
				cd = new Cooldown(p.getUniqueId(), "" + id, Math.round(cooldown * 0.8f));
			}
			else {
				cd = new Cooldown(p.getUniqueId(), "" + id, cooldown);
			}
		}
		cd.start();
		if (!Main.Ecaflip.ecaspell.containsKey(p.getUniqueId()) || slot < 3) {
			ItemStack s2Clone = getIconCd();
			s2Clone.setAmount(Cooldown.getTimeLeft(p.getUniqueId(), "" + id));
    		p.getInventory().setItem(100 + slot, s2Clone);
    		new BukkitRunnable() {
    			Personnage perso = Players.getPersonnage(p);
    		    @Override
    		    public void run() {
    		    	if (!p.isDead()) {
    		    		if (Players.getPersonnage(p) != perso) {
    		    			cancel();
    		    			return;
    		    		}
    		    		s2Clone.setAmount(Cooldown.getTimeLeft(p.getUniqueId(), "" + id));
        	    		p.getInventory().setItem(100 + slot, s2Clone);
        	    		if (!Cooldown.isInCooldown(p.getUniqueId(), "" + id)) {
        	        		p.getInventory().setItem(100 + slot, getIcon());
        	    			cancel();
        	    		}
    		    	}
    		    }
    		}.runTaskTimer(Main, 0L, 20L);
		}
	}
	
	public void triggerCooldown(Player p) {
		this.cooldown(p);
	}
	
	public void resetCooldown(Player p) {
		if (cooldown > 0) {
			Cooldown cd;
			cd = new Cooldown(p.getUniqueId(), "" + id, 0);
			cd.start();
		}
	}
	
	public boolean isInCooldown(Player p) {
		return Cooldown.isInCooldown(p.getUniqueId(), "" + id);
	}
	
	//this gets the entity the player is looking at or null if no valid target is found
	//kinda complicated to explain every interaction
	protected LivingEntity getTarget(Player p) {
		int porteeTotale = portee + Players.getPortee(p);
		for(Entity e : p.getNearbyEntities(porteeTotale, porteeTotale, porteeTotale)){
			if(e instanceof LivingEntity){
				LivingEntity pp = (LivingEntity) e;
				if(getLookingAt(p, pp, porteeTotale) == true && !pp.hasMetadata("NPC") && !pp.isDead()) {
					if (Main.Sram.invi.contains(e.getUniqueId()) || Main.Ecaflip.poofed.contains(e.getUniqueId()) || Main.brp.contains(e.getUniqueId())) {
						return null;
					}
					else if (e instanceof Player && p.getWorld().getName().contains("incarnam")) {
						p.sendMessage(ChatColor.DARK_RED + "Vous ne pouvez pas utiliser de sorts sur les autres joueurs tant que vous êtes sur Incarnam !");
						return null;
					}
					else if (e instanceof Player && e.hasPermission("spawn")) {
						p.sendMessage(ChatColor.DARK_RED + "Vous ne pouvez pas utiliser de sorts sur les joueurs étant dans le spawn !");
						return null;
					}
					else if (e instanceof Player && ((Player)e).getGameMode() == GameMode.SPECTATOR) {
						return null;
					}
					else if(e.hasMetadata("iopduel")) {
						if (Main.Iop.iopduel.get(e) != p) {
							return null;
						}
						else if (e instanceof ArmorStand) {
							if (e.hasMetadata("boss")) {
								return (LivingEntity) e.getMetadata("boss").get(0).value();
							}
							else {
								return null;
							}
						}
						else if (e instanceof Slime && e.isInsideVehicle()) {
							return null;
						}
						else return (LivingEntity) e;
					}
					else if (e instanceof ArmorStand) {
						if (e.hasMetadata("boss")) {
							return (LivingEntity) e.getMetadata("boss").get(0).value();
						}
						else {
							return null;
						}
					}
					else if (e instanceof Slime && e.isInsideVehicle()) {
						return null;
					}
					else return (LivingEntity) e;
				}
			}
		}
		return null;
	}

	//this is how we check if an entity is actually in the direct line of sight of a player
	private boolean getLookingAt(Player player, LivingEntity pp, int d){
		if (!player.hasLineOfSight(pp)) { //if there's an obstruction between both entities
			return false;
		}
		double x = 0; //this is a value that gets tweaked depending on different conditions to make the check as accurate as possible
		double distance = player.getLocation().distance(pp.getLocation());
		if (Main.Pandawa.picole.contains(player.getUniqueId()) && !Main.Pandawa.stabi.contains(player.getUniqueId())) {
			if (distance > 10) {
				x = 0.989D;
			}
			else if (distance > 5) {
				x = 0.98D;
			}
			else if (distance < 2){
				x = 0.8D;
			}
			else {
				x = 0.97D;
			}
		}
		else {
			if (distance > 10) {
				x = 0.999D;
			}
			else if (distance > 5) {
				x = 0.99D;
			}
			else if (distance < 2){
				x = 0.9D;
			}
			else {
				x = 0.98D;
			}
		}
		
		//this is a bit messy to explain but we pretty much need to check the alignment of the player's eye vector with 4 points :
		// - the location of the entity (which is at the bottom of it's hitbox)
		// - the location of the entity + half of it's height (so mid hitbox)
		// - the location of the entity's eyes
		// - the location of the entity + it's height (so top of it's hitbox)
		Location eye = player.getEyeLocation();
		Location entityLocationB = pp.getLocation();
		Location entityLocationM = pp.getLocation();
		Location entityLocationT = pp.getLocation();
		Location entityLocationE = pp.getEyeLocation();
		entityLocationM.setY(entityLocationM.getY() + pp.getHeight() / 2);
		entityLocationT.setY(entityLocationT.getY() + pp.getHeight());
		Vector toEntity1 = entityLocationB.toVector().subtract(eye.toVector());
		Vector toEntity2 = entityLocationM.toVector().subtract(eye.toVector());
		Vector toEntity3 = entityLocationT.toVector().subtract(eye.toVector());
		Vector toEntity4 = entityLocationE.toVector().subtract(eye.toVector());
		double dot = toEntity1.normalize().dot(eye.getDirection());
		double dot2 = toEntity2.normalize().dot(eye.getDirection());
		double dot3 = toEntity3.normalize().dot(eye.getDirection());
		double dot4 = toEntity4.normalize().dot(eye.getDirection());

		if (dot > x || dot2 > x || dot3 > x || dot4 > x) {
			return true;	
		}
		else
			return false;
	}

	//this inputs a message in chat for every player in a 25 bloc radius around the caster
	//and the caster itself to let everyone know a spell was casted
	protected void callout(Player p, String spell) { 
 		if (!p.getNearbyEntities(25, 25, 25).isEmpty()) {
 			 for (int i = 0; i < p.getNearbyEntities(25, 25, 25).size(); i++) {
 	            if(p.getNearbyEntities(25, 25, 25).get(i) instanceof Player){
 	            	Player pp = (Player) p.getNearbyEntities(25, 25, 25).get(i);
 	            	if (p.hasLineOfSight(pp)) {
 	            			 pp.sendMessage(ChatColor.WHITE + p.getName() + " utilise " + ChatColor.YELLOW + spell + ChatColor.WHITE + " !");
 	            	}
 	            }
 			 }
 		}
 		p.sendMessage(ChatColor.WHITE + p.getName() + " utilise " + ChatColor.YELLOW + spell + ChatColor.WHITE + " !");
	}
}
