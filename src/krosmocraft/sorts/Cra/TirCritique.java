package krosmocraft.sorts.Cra;


import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import krosmocraft.interactions.main;
import krosmocraft.sorts.Sort;

public class TirCritique extends Sort {
	
	public TirCritique(main Main) {
		super(Main);
		
		name = "Tir Critique";

		lore.add(ChatColor.GRAY + "[PASSIF]");
		lore.add(ChatColor.GRAY + "Augmente le multiplicateur de Tir Puissant");
		lore.add(ChatColor.GRAY + "jusqu'à 4 mais double le cooldown");
		
		slot = 3;
		
		id = 7;
		
		portee = -1;
		
		cooldown = -1;
		
		create();
	}

	@Override
	protected boolean cast(Player p) {
		return false;
	}
	
}
