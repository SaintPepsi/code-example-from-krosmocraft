package krosmocraft.sorts.Cra;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.AbstractArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.SpectralArrow;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.scheduler.BukkitRunnable;

import krosmocraft.interactions.main;
import krosmocraft.krosmobjects.Players;
import krosmocraft.sorts.Sort;

public class TirPuissant extends Sort implements Listener {
	
	public TirPuissant(main Main) {
		super(Main);
		
		name = "Tir Puissant";
		
		lore.add(ChatColor.GRAY + "Multiplie les dégâts de la prochaine flèche tirée par 2");
		lore.add(ChatColor.GRAY + "Ce boost dûre 6 secondes et possède");
		lore.add(ChatColor.GRAY + "10 secondes de cooldown");
		
		slot = 0;
		
		id = 1;

		portee = -1;
		
		cooldown = 10;
		
		create();
	}

	@Override
	protected boolean cast(Player p) {
		callout(p, name);
		if (!Main.Cra.powershot.contains(p.getUniqueId())) {
			Main.Cra.powershot.add(p.getUniqueId());
		}
        cooldown = 10;
        if (Players.checkForSpell(p, Main.Cra.TirCritique)) {
        	cooldown = 20;
        }
		p.getWorld().playSound(p.getLocation(), "puissant", 1.6F, 1.0F);
    	BossBar s1 = Bukkit.getServer().createBossBar(ChatColor.GREEN + "Tir Puissant", BarColor.GREEN, BarStyle.SOLID);
        s1.addPlayer(p);
        s1.setVisible(true);
    	s1.setProgress(1.0D);
		new BukkitRunnable() {
		    @Override
		    public void run() {
		    	s1.setProgress(s1.getProgress() - 0.0083);
		    	if ((s1.getProgress() <= 0.01D) || !Main.Cra.powershot.contains(p.getUniqueId())) {
	    			if (Main.Cra.powershot.contains(p.getUniqueId())) {
	    				Main.Cra.powershot.remove(p.getUniqueId());
					}
		    		s1.removeAll();
		    		cancel();
		    	}
	    	}
		}.runTaskTimer(Main, 1L, 1L);
		return true;
	}

	@EventHandler
	public void shotBuffs(EntityShootBowEvent e) {
		if (e.isCancelled()) {
			return;
		}
		else if (e.getProjectile() instanceof Arrow || e.getProjectile() instanceof SpectralArrow) {
			AbstractArrow a = (AbstractArrow) e.getProjectile();
			if (a.getShooter() instanceof Player) {
				Player p = (Player) a.getShooter();
				if (Main.Cra.powershot.contains(p.getUniqueId())) {
					Main.Cra.powershot.remove(p.getUniqueId());
					a.setCustomName(a.getCustomName() + "powerarrow");
				}
			}
		}
	}

	@EventHandler
	public void onArrowDamage(EntityDamageByEntityEvent e) {
		Player p = null;
		if (e.getDamager() instanceof Projectile) {
			Projectile po = (Projectile) e.getDamager();
			if (po.getShooter() instanceof Player) {
				p = (Player) po.getShooter();
			}
		}
		if (p != null) {
			if (e.getDamager() instanceof Arrow || e.getDamager() instanceof SpectralArrow) {
				AbstractArrow arrow = (AbstractArrow) e.getDamager();
				if (arrow.getCustomName() != null && str.contains("powerarrow")) {
					if (!Players.checkForSpell(p, Main.Cra.TirCritique)) {
						e.setDamage(e.getDamage() * 2);
					}
					else {
						e.setDamage(e.getDamage() * 4);
					}
				}
			}
		}
	}
}
